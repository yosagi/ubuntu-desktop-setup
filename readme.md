# Ubuntu開発環境セットアップツール

## Quick start

ansibleもインストールされていない環境ならばまずansibleをインストールする。

``` bash
 ./bootstrap.sh
```

何も気にせずvirtualBox以外一通りインストールするなら

``` bash
 ./setup.sh
```

基本的な環境とvs codeだけ入れば良いなら

``` bash
 ansible-playbook -K all.yml --tags base,vscode
```

基本的な環境、emacs、rtagsが入れば良いなら

``` bash
 ansible-playbook -K all.yml --tags base,emacs
```

などとする。

## all.yml と tags
all.yml にはすべてのrolesが列挙されており、それぞれtagが設定されている。tagはrole名と、もしあれば関連するカテゴリ名がある。
カテゴリ名は今の所以下のもの

- emacsdev
- spacemacs
- vscode-user

すべてのroleで同名のtagを設定しているので、それぞれ個別に指定できる。カテゴリ名は依存関係にあるroleを簡単にまとめて指定できるようにするために用意した。spacemacsはrole名でもあるが、emacs roleにもつけてある。vscode-user

all.ymlに列挙されたroleとそれが実行するtask及び設定されたtagを列挙するには、

```bash
ansible-playbook all.yml --list-tasks
```

とする。また、どのタスクが実行されるのか確認するには

```bash
ansible-playbook all.yml --tags spacemacs,user --check
```

などとする。(--checkをつける)

## roles

### base

- build-essential
- cmake
- vim
- jed
- git
- ag (silver searcher)
- rg (ripgrep)
- byobu
- zeal
- lv

### emacs

- emacs
- emacs-mozc
- migmix font
- ricty diminished font

### emacsdev

- clang
- global

### rtags

- rtags
- rtags のビルドに必要なパッケージ群
- rdm を自動起動するユーザ設定

### spacemacs

- spacemacs
- yos layers

### vscode

- vscode

### vscode-user

- vscodeの以下の拡張機能のインストール
  - AlanWalk.markdown-toc
  - DavidAnson.vscode-markdownlint
  - geddski.macros
  - hnw.vscode-auto-open-markdown-preview
  - IBM.output-colorizer
  - lfs.vscode-emacs-friendly
  - mitaki28.vscode-clang
  - ms-vscode.cpptools
  - selbh.keyboard-scroll
  - twxs.cmake
  - vector-of-bool.cmake-tools
  - xaver.clang-format
  - silverlakesoftware.searchdocsets-vscode
- C-o (open line) と M-g (goto line)のキーバインド

### atom

- atom

### ros

- ros melodic desktop
- tf2 geometry msgs
- tf2 sensor msgs
- rosdep
- ros環境設定スクリプトを起動する rosenv.sh スクリプト

うまく行かない場合鍵を更新
```
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
```


### panda3d

- panda 3d

### fish

- fish shell
- fisherman
- fzy
- fzy plugin for fish

### userconfig

- git credential の cache設定
- git ssl verify を無効に
- screenrc
- alias vv(vim のlessマクロ)

### virtualBox

- virtualbox guest utils
- virtualbox guest x11

### tensorflow-deps

- bazel

### layzygit

- lazygit

### gitui

cargoでgituiをインストールする。rust 1.50 が必要なのでrust role で最新のrustをインストールする

- gitui

### rust

rust公式配布のインストールスクリプトで最新のrustを~/.localにインストールする。ルート権限は不要。

### intel
Intel OneAPIのbasekit, hpckit, dlfdkit, aikit
- intel-basekit
- intel-hpckit
- intel-dlfdkit
- intel-aikit

