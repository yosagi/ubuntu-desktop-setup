#!/bin/bash

# trusty では ansible-ansible-trusty.list
# xenial では ansible-ubuntu-ansible-trusty.list
# bionic では ansible 2.5 が入る
# focal では  ansible 2.9 が入る
osrel=$(lsb_release -c -s)
if [ "$osrel" == "xenial" ]; then
    ostype="-ubuntu"
fi
if [ "$osrel" == "bionic" -o "$osrel" == "focal" ]; then
    sudo apt-get -y install ansible python3-lxml
    exit
fi

# install latest ansible
if [ ! -f /etc/apt/sources.list.d/ansible${ostype}-ansible-${osrel}.list ]; then

    echo "Installing ppa dependency"
    sudo apt install software-properties-common
    echo "Adding ansible ppa repository"
    sudo apt-add-repository -y ppa:ansible/ansible
    echo "Updating package info"
    sudo apt-get update
    echo "Installing ansible"
    sudo apt-get -y install ansible
    echo "Installing lxml"
    sudo apt-get -y install python-lxml
fi
