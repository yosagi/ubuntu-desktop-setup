#!/bin/bash  --init-file

# load standard init files
if [ -r /etc/profile ]; then
. /etc/profile
fi
if [ -r ~/.bash_profile ]; then
 source ~/.bash_profile
elif [ -r ~/.bash_login ]; then
 source ~/.bash_login
elif [ -r ~/.profile ]; then
 source ~/.profile
fi

function searchCatkinWS()
{
  if [ -r $1/.catkin_tools/README ]; then
    echo $1
    return 0
  fi
  if [ "$1" == "/" ]; then
    return 1
  fi
  searchCatkinWS $(dirname $1)
  return $?
}

if [ -z "${ROS_DISTRO}" ]; then
  case $(lsb_release -cs) in
    focal )
    ROS_DISTRO=noetic
    ;;

    bionic )
    ROS_DISTRO=melodic
    ;;

    xenial )
    ROS_DISTRO=kinetic
    ;;
  esac
fi

SITE_ROS=/opt/ros
DEFAULT_ROS=${SITE_ROS}/${ROS_DISTRO}

if [ ! -d $DEFAULT_ROS ]; then
 echo "No ROS installation found at default location : $DEFAULT_ROS"
 exit
fi

ws=$(searchCatkinWS $(pwd -P))
if [ -n "$ws" -a -r $ws/devel/setup.bash ]; then
  echo "Loading environment in [$ws]"
  source $ws/devel/setup.bash
  export PS1="[$ws] $PS1"
  export HISTFILE=$ws/.bash_history
else
  echo "Loading default environment in [$DEFAULT_ROS]"
  source ${DEFAULT_ROS}/setup.bash
  export PS1="[${DEFAULT_ROS}] $PS1"
fi
echo launching new interactive shell with ROS environment
